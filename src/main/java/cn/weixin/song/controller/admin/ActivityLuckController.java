package cn.weixin.song.controller.admin;

import java.util.HashSet;
import java.util.Set;

import cn.weixin.song.model.Activity;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jfinal.plugin.activerecord.Page;


@RequiresPermissions(value={"/activity/luck"})
public class ActivityLuckController extends JCBaseController {
    

	
	public void index() {
		render("index.jsp");
	}
	
	public void getListData() {
		String name=this.getPara("name");
		Set<Condition> conditions=new HashSet<Condition>();
		if(CommonUtils.isNotEmpty(name)){
			conditions.add(new Condition("name",Operators.LIKE,name));
		}
		Page<Activity> pageInfo=Activity.dao.getPage(this.getPage(), this.getRows(),conditions,this.getOrderby());
		this.renderJson(Activity.dao.toJqGridView(pageInfo)); 
	}


}
